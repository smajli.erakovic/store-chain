<?php

namespace Src;

class BillNumber
{
    private $number = 1;
    private $year;

    public function __construct()
    {
        $this->year = date('Y');
    }

    public function generate()
    {
        $currentYear = date('Y');

        if ($this->year !== $currentYear) {
            $this->number = 0;
            $this->year = $currentYear;
        }

        return $this->number++ . $this->year;
    }
}
