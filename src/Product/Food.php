<?php

namespace Src\Product;

use Src\Interfaces\ProductInterface;
use Src\Product\Product;
use Src\Product\ProductTypes;

class Food extends Product implements ProductInterface
{
    public function __construct($name, $price)
    {
        parent::__construct($name, ProductTypes::FOOD, $price);
    }
}