<?php

namespace Src\Interfaces;

interface CustomerInterface
{
    /**
     * Get first name
     *
     * @return mixed
     */
    public function getFirstName();

    /**
     * Set first name
     *
     * @param mixed $firstName
     * @return Customer
     */
    public function setFirstName($firstName);

    /**
     * Get Last name
     *
     * @return mixed
     */
    public function getLastname();

    /**
     * Set Last name
     *
     * @param mixed $lastname
     * @return Customer
     */
    public function setLastname($lastname);

    /**
     * Get phone number
     *
     * @return mixed
     */
    public function getPhoneNumber();

    /**
     * Set phone number
     *
     * @param mixed $phoneNumber
     * @return Customer
     */
    public function setPhoneNumber($phoneNumber);
}