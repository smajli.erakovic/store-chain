<?php

namespace Src\Interfaces;

interface SpecialProductInterface
{
    public function getSerialNumber();
}