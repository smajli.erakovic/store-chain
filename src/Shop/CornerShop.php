<?php

namespace Src\Shop;

use Src\Interfaces\ShopInterface;
use Src\Stock;

class CornerShop extends Shop implements ShopInterface
{
    /**
     * Create Corner Shop
     *
     * @param $name
     * @param Stock $stock
     */
    public function __construct($name, Stock $stock)
    {
        parent::__construct($name, ShopTypes::CORNER_SHOP, $stock);
    }
}