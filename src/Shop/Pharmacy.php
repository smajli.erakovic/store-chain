<?php

namespace Src\Shop;

use Src\Interfaces\ShopInterface;
use Src\Stock;

class Pharmacy extends Shop implements ShopInterface
{
    /**
     * Create Pharmacy
     *
     * @param $name
     * @param Stock $stock
     */
    public function __construct($name, Stock $stock)
    {
        parent::__construct($name, ShopTypes::PHARMACY, $stock);
    }
}