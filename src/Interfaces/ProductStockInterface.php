<?php

namespace Src\Interfaces;

interface ProductStockInterface
{
    /**
     * @return mixed
     */
    public function getProduct();

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * Decrement quantity,
     *
     * @param int $quantity
     *
     * @return bool
     */
    public function decQuantity(int $quantity = 1);

    /**
     * Increment quantity
     *
     * @param int $quantity
     * @return ProductStock
     */
    public function incQuantity(int $quantity);

    /**
     * Get serial number
     *
     * @return mixed
     */
    public function getSerialNumber();
}