<?php

namespace Src;

class DateFormat
{
    const DATE = 'Y-m-d';
    const TIME = 'H:m:i';
    const DATETIME = self::DATE . ' ' . self::TIME;
}