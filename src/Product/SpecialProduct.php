<?php

namespace Src\Product;

use Src\Interfaces\SpecialProductInterface;

abstract class SpecialProduct extends Product implements SpecialProductInterface
{
    private $serialNumber;

    public function __construct($name, $type, $price)
    {
        parent::__construct($name, $type, $price);
        $this->serialNumber = hash('sha256', rand());
    }

    public function getSerialNumber()
    {
        return $this->serialNumber;
    }
}