<?php

namespace Src;

use Src\Interfaces\CustomerInterface;

class Customer implements CustomerInterface
{
    private $firstName;
    private $lastname;
    private $phoneNumber;

    /**
     * Customer constructor
     *
     * @param $firstName
     * @param $lastname
     * @param $phoneNumber
     */
    public function __construct($firstName, $lastname, $phoneNumber)
    {
        $this->firstName = $firstName;
        $this->lastname = $lastname;
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return Customer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     * @return Customer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }
}