<?php

namespace Src\Product;

use ReflectionClass;

class ProductTypes
{
    const FOOD = 'food';
    const DRINK = 'drink';
    const MEDICINE = 'medicine';
    const CIGARETTE = 'cigarette';
    const TOY = 'toy';
    const PARKING_TICKET = 'parking ticket';

    public static function getTypes()
    {
        $types = new ReflectionClass(static::class);

        return $types->getConstants();
    }
}