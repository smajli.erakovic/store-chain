<?php

require "bootstrap.php";

use Src\Test;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers");

$test = new Test();

$test->pharmacyShopTest();
$test->cornerShopTest();
$test->supermarketTest();
