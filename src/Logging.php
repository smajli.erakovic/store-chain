<?php

namespace Src;

use Carbon\Carbon;

class Logging {
    private $logFile;
    private $fp;

    // set log file (path and name)
    public function lfile($path) {
        $this->logFile = $path;
    }
    // write message to the log file
    public function lwrite($message) {
        // if file pointer doesn't exist, then open log file
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        // define script name
        $scriptName = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        // define current time and suppress E_WARNING if using the system TZ settings
        // (don't forget to set the INI setting date.timezone)
        $time = date(DateFormat::DATETIME);
        // write current time, script name and message to the log file
        fwrite($this->fp, "$time ($scriptName) $message" . PHP_EOL);
    }
    // close log file (it's always a good idea to close a file when you're done with it)
    public function lclose() {
        fclose($this->fp);
    }
    // open log file (private method)
    private function lopen() {
        // define log file from lfile method or use previously set default
        $lfile = $this->logFile ? $this->logFile : './logfile.txt';
        // open log file for writing only and place file pointer at the end of the file
        // (if the file does not exist, try to create it)
        $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
    }
}