<?php


namespace Src;

use Exception;
use Src\Product\Cigarette;
use Src\Product\Drink;
use Src\Product\Food;
use Src\Product\Medicine;
use Src\Product\ProductStock;
use Src\Shop\CornerShop;
use Src\Shop\Pharmacy;
use Src\Shop\Supermarket;

class Test
{
    public function pharmacyShopTest()
    {
        try {
            // Pharmacy shop test
            $shop = new Pharmacy('Jankovic', new Stock());

            $panadol = new Medicine('Panadol', 1320);
            $shop->add(new ProductStock($panadol, 20));

            $cornflake = new Food('Cornflake', 820);
            $shop->add(new ProductStock($cornflake, 30));

            $customer = new Customer('John', 'Doe', '+38164233430');
            $cart = new Cart($shop, $customer);
            $cart->add($panadol, 3);
            $cart->add($cornflake);
            $cart->purchase();

            $customer = new Customer('Jane', 'Doe', '+38165253430');
            $cart = new Cart($shop, $customer);
            $cart->add($cornflake, 2);
            $cart->purchase();

            $customer = new Customer('Marry', 'Doe', '+38165253430');
            $cart = new Cart($shop, $customer);
            $cart->add($cornflake, 30);
            $cart->purchase();

            echo $shop->getReportFor(date(DateFormat::DATE));
        } catch (Exception $e) {
            echo $e->getMessage() . "\n\n";
        }
    }

    public function cornerShopTest()
    {
        try {
            $shop = new CornerShop('Trafikica', new Stock());

            $cigarette = new Cigarette('malboro', 410);
            $shop->add(new ProductStock($cigarette, 50));

            $chips = new Food('chips', 520);
            $shop->add(new ProductStock($chips, 30));

            $customer = new Customer('John', 'Doe', '+38164233430');
            $cart = new Cart($shop, $customer);
            $cart->add($chips, 3);
            $cart->add($cigarette, 2);
            $cart->purchase();

            $customer = new Customer('Jane', 'Doe', '+38164235310');
            $cart = new Cart($shop, $customer);
            $cart->add($chips);
            $cart->purchase();

            $customer = new Customer('Marry', 'Doe', '+38165253430');
            $cart = new Cart($shop, $customer);
            $cart->add($chips, 30);
            $cart->purchase();

            echo $shop->getReportFor(date(DateFormat::DATE));
        } catch (Exception $e) {
            echo $e->getMessage() . "\n\n";
        }
    }

    public function supermarketTest()
    {
        try {
            $shop = new Supermarket('Mikromarket', new Stock());

            $beer = new Drink('Beer', 150);
            $shop->add(new ProductStock($beer, 30));

            $plazma = new Food('plazma', 20);
            $shop->add(new ProductStock($plazma, 10));

            $customer = new Customer('John', 'Doe', '+38164233430');
            $cart = new Cart($shop, $customer);
            $cart->add($beer, 2);
            $cart->add($plazma, 5);
            $cart->purchase();

            $customer = new Customer('Jane', 'Doe', '+38164235310');
            $cart = new Cart($shop, $customer);
            $cart->add($plazma);
            $cart->purchase();

            $customer = new Customer('Marry', 'Doe', '+38165253430');
            $cart = new Cart($shop, $customer);
            $cart->add($plazma, 20);
            $cart->purchase();

            echo $shop->getReportFor(date(DateFormat::DATE));
        } catch (Exception $e) {
            echo $e->getMessage() . "\n\n";
        }
    }
}