<?php

namespace Src\Shop;

use Src\Interfaces\ShopInterface;
use Src\Stock;

class Supermarket extends Shop implements ShopInterface
{
    /**
     * Create Supermarket
     *
     * @param $name
     * @param Stock $stock
     */
    public function __construct($name, Stock $stock)
    {
        parent::__construct($name, ShopTypes::SUPERMARKET, $stock);
    }
}