<?php

namespace Src\Interfaces;

use Src\Customer;
use Src\Product\ProductStock;

interface ShopInterface
{
    /**
     * @return BillNumber
     */
    public function getBillNumber();

    /**
     * @param BillNumber $billNumber
     * @return Shop
     */
    public function setBillNumber($billNumber);

    /**
     * @return Stock
     */
    public function getStock();

    /**
     * @param Stock $stock
     * @return Shop
     */
    public function setStock($stock);

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param mixed $name
     * @return Shop
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param mixed $type
     * @return Shop
     */
    public function setType($type);

    /**
     * Add Product Stock to the Stock
     *
     * @param ProductStockInterface $productStock
     */
    public function add(ProductStockInterface $productStock);

    /**
     * @return array
     */
    public function getBills();

    /**
     * @param Bill $bill
     *
     * @return Shop
     */
    public function createBill(Customer $customer, array $productStocks);

    public function getReportFor($date);
}