<?php

namespace Src\Product;

use Src\Interfaces\ProductInterface;
use Src\Interfaces\SpecialProductInterface;
use Src\Product\ProductTypes;
use Src\Product\SpecialProduct;

class ParkingTicket extends SpecialProduct implements ProductInterface, SpecialProductInterface
{
    public function __construct($name, $price)
    {
        parent::__construct($name, ProductTypes::PARKING_TICKET, $price);
    }
}