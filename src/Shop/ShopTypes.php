<?php

namespace Src\Shop;

use ReflectionClass;

class ShopTypes
{
    const CORNER_SHOP = 'corner shop';
    const SUPERMARKET = 'supermarket';
    const PHARMACY = 'pharmacy';

    public static function getTypes()
    {
        $types = new ReflectionClass(static::class);

        return $types->getConstants();
    }
}