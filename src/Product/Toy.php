<?php

namespace Src\Product;

use Src\Interfaces\ProductInterface;

class Toy extends Product implements ProductInterface
{
    /**
     * Create Toy
     *
     * @param $name
     * @param $price
     */
    public function __construct($name, $price)
    {
        parent::__construct($name, ProductTypes::TOY, $price);
    }
}