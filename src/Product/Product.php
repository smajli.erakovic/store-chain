<?php

namespace Src\Product;

use Exception;
use Src\Product\ProductTypes;

abstract class Product
{
    private $type;
    private $name;
    private $price;

    /**
     * Create Product
     *
     * @param $name
     * @param $type
     * @param $price
     *
     * @throws Exception
     */
    public function __construct($name, $type, $price)
    {
        if (!in_array($type, ProductTypes::getTypes())) {
            throw new Exception('Invalid Product type');
        }

        $this->name = $name;
        $this->type = $type;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}