<?php

namespace Src;

use Exception;
use Src\Interfaces\ProductInterface;
use Src\Interfaces\ShopInterface;
use Src\Product\Product;
use Src\Product\ProductStock;

class Cart
{
    private $shop;
    private $customer;
    private $productStocks = [];
    private $log;

    public function __construct(ShopInterface $shop, Customer $customer)
    {
        $this->shop = $shop;
        $this->customer = $customer;
        $this->log = new Logging();
    }

    /**
     * Add Product to Cart
     *
     * @param Product $product
     *
     * @return $this
     */
    public function add(ProductInterface $product, int $quantity = 1)
    {
        try {
            $isSold = $this->shop->getStock()->decQuantity($product, $quantity);

            if ($isSold === false) {
                throw new Exception(
                    "Invalid purchase of a $quantity product, " .
                    "the remaining quantity of products would be less than zero"
                );
            }

            $this->productStocks[] = new ProductStock($product, $quantity);
        } catch (Exception $e) {
            $this->emptyCart();
            echo $e->getMessage() . "\n\n";
        }

        return $this;
    }

    public function purchase()
    {
        if ($this->productStocks) {
            $this->shop->createBill($this->customer, $this->productStocks);
            $this->logInfo();

            $this->productStocks = [];
        }
    }

    public function emptyCart()
    {
        foreach ($this->productStocks as $productStock) {
            $this->shop->getStock()->incQuantity($productStock->getProduct(), $productStock->getQuantity());
        }

        $this->productStocks = [];
    }

    private function logInfo()
    {
        foreach ($this->productStocks as $prodStock) {
            $this->log->lwrite(
                "{$this->shop->getType()}, {$prodStock->getProduct()->getType()}, {$prodStock->getProduct()->getPrice()}, " .
                ($this->shop->getStock()->getProductStock($prodStock->getProduct())->getQuantity() + $prodStock->getQuantity() . ", ") .
                "{$this->shop->getStock()->getProductStock($prodStock->getProduct())->getQuantity()} "
            );
        }

        $this->log->lclose();
    }
}
