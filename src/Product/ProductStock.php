<?php

namespace Src\Product;

use Src\Interfaces\ProductInterface;
use Src\Interfaces\ProductStockInterface;
use Src\Interfaces\SpecialProductInterface;

class ProductStock implements ProductStockInterface
{
    private $product;
    private $quantity;

    /**
     * Create Product Stock
     *
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(ProductInterface $product, int $quantity = 1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Decrement quantity
     *
     * @param int $quantity
     *
     * @return bool
     */
    public function decQuantity(int $quantity = 1)
    {
        if ($isSold = $this->quantity >= $quantity) {
            $this->quantity -= $quantity;
        }

        return $isSold;
    }

    /**
     * Increment quantity
     *
     * @param int $quantity
     * @return ProductStock
     */
    public function incQuantity(int $quantity)
    {
        $this->quantity += $quantity;

        return $this;
    }

    public function getSerialNumber()
    {
        if ($this->getProduct() instanceof SpecialProductInterface) {
            return $this->getProduct()->getSerialNumber();
        }

        return null;
    }
}