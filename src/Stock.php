<?php

namespace Src;

use Exception;
use Src\Interfaces\ProductInterface;
use Src\Interfaces\ProductStockInterface;
use Src\Interfaces\ShopInterface;
use Src\Product\Product;
use Src\Product\ProductStock;
use Src\Product\ProductTypes;
use Src\Shop\Shop;
use Src\Shop\ShopTypes;

class Stock
{
    private $productStocks = [];

    /**
     * Add Product Stock
     *
     * @param ProductStock $productStock
     */
    public function add(ProductStockInterface $productStock, ShopInterface $shop)
    {
        $product = $productStock->getProduct();

        if ($this->productExists($product)) {
            throw new Exception('Product type ' . $product->getType() . ' already exists');
        }

        $this->validateProduct($productStock, $shop);

        // insert product into productStock array (where key is Product type, and value is ProductStock)
        $this->productStocks[$productStock->getProduct()->getType()] = $productStock;
    }

    /**
     * Decrement Product quantity in Product Stock
     *
     * @param Product $product
     * @param int $quantity
     * @return mixed
     * @throws Exception
     */
    public function decQuantity(ProductInterface $product, int $quantity = 1)
    {
        return $this->getProductStock($product)->decQuantity($quantity);
    }

    /**
     * Increment Product quantity in Product Stock
     *
     * @param Product $product
     * @param int $quantity
     * @return mixed
     * @throws Exception
     */
    public function incQuantity(ProductInterface $product, int $quantity = 1)
    {
        return $this->getProductStock($product)->incQuantity($quantity);
    }

    /**
     * Get Product Stock by Product type
     *
     * @param Product $product
     * @return mixed
     * @throws Exception
     *
     */
    public function getProductStock(ProductInterface $product)
    {
        if (!$this->productExists($product)) {
            throw new Exception('Product type ' . $product->getType() . ' does not exists');
        }

        return $this->productStocks[$product->getType()];
    }

    private function productExists(ProductInterface $product)
    {
        return array_key_exists($product->getType(), $this->productStocks);
    }

    /**
     * Validate Product
     *
     * @param ProductStock $productStock
     * @param Shop $shop
     * @throws Exception
     */
    private function validateProduct(ProductStockInterface $productStock, Shop $shop)
    {
        $isInvalid = ($productStock->getProduct()->getType() == ProductTypes::CIGARETTE && $shop->getType() != ShopTypes::CORNER_SHOP);
        if ($isInvalid) {
            throw new Exception('Can not add this product to the shop, ' . ProductTypes::CIGARETTE . ' can be sold only by ' . ShopTypes::CORNER_SHOP);
        }

        $isInvalid = ($productStock->getProduct()->getType() == ProductTypes::MEDICINE && $shop->getType() != ShopTypes::PHARMACY);
        if ($isInvalid) {
            throw new Exception('Can not add this product to the shop, ' . ProductTypes::MEDICINE . ' can be sold only by ' . ShopTypes::PHARMACY);
        }
    }
}