<?php

namespace Src;

class Bill
{
    private $number;
    private $createdAt;
    private $customer;
    private $productStocks = [];

    public function __construct(BillNumber $billNumber, Customer $customer, array $productStocks)
    {
        $this->productStocks = $productStocks;
        $this->number = $billNumber->generate();
        $this->customer = $customer;
        $this->createdAt = date(DateFormat::DATETIME);
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     *
     * @return Bill
     */
    public function setNumber(string $number): Bill
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return false|string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param false|string $createdAt
     * @return Bill
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return Bill
     */
    public function setCustomer(Customer $customer): Bill
    {
        $this->customer = $customer;
        return $this;
    }

    public function getInfo()
    {
        $info = "Bill number: " . $this->number . ",\nCreated at " . $this->createdAt . "\n" .
            "Customer: " . $this->customer->getFirstName() . " " .
            $this->customer->getLastname() .
            ", phone number " . $this->customer->getPhoneNumber() . "\n\nProduct list:\n\n";
        foreach ($this->productStocks as $i => $stock) {
            $info .= $i + 1 . ". " . $stock->getProduct()->getType() . ": " . $stock->getProduct()->getName() .
                " (quantity: " . $stock->getQuantity() . ", price: " . $stock->getProduct()->getPrice();
            if ($serialNumber = $stock->getSerialNumber()) {
                $info .= ", serial number: $serialNumber";
            }
            $info .= ")\n";
        }

        return "$info------------\n\n";
    }
}
