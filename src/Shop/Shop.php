<?php

namespace Src\Shop;

use Exception;
use Carbon\Carbon;
use Src\Bill;
use Src\BillNumber;
use Src\Customer;
use Src\DateFormat;
use Src\Interfaces\ProductStockInterface;
use Src\Interfaces\ShopInterface;
use Src\Product\ProductStock;
use Src\Stock;

abstract class Shop implements ShopInterface
{
    private $type;
    private $name;
    private $bills = [];
    private $billNumber;
    private $stock;

    /**
     * Create Shop
     *
     * @param $name
     * @param $type
     * @param Stock $stock
     * @throws Exception
     */
    public function __construct($name, $type, Stock $stock)
    {
        if (!in_array($type, ShopTypes::getTypes())) {
            throw new Exception('Invalid Shop type');
        }

        $this->name = $name;
        $this->type = $type;
        $this->billNumber = new BillNumber();
        $this->stock = $stock;
    }

    /**
     * @return BillNumber
     */
    public function getBillNumber()
    {
        return $this->billNumber;
    }

    /**
     * @param BillNumber $billNumber
     *
     * @return Shop
     */
    public function setBillNumber($billNumber)
    {
        $this->billNumber = $billNumber;

        return $this;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     *
     * @return Shop
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Shop
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Add Product Stock to the Stock
     *
     * @param ProductStock $productStock
     */
    public function add(ProductStockInterface $productStock)
    {
        $this->stock->add($productStock, $this);
    }

    /**
     * @return array
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * @param Bill $bill
     *
     * @return Shop
     */
    public function createBill(Customer $customer, array $productStocks)
    {
        $bill = new Bill($this->getBillNumber(), $customer, $productStocks);

        $this->bills[] = $bill;

        return $this;
    }

    /**
     * Get report by date
     *
     * @param $date
     */
    public function getReportFor($date)
    {
        $date = Carbon::createFromFormat(DateFormat::DATE, $date);
        $billInfo = "=======================================\n" .
            "Shop {$this->getName()} report on {$date->format(DateFormat::DATE)}: \n" .
            "=======================================\n\n";

        foreach ($this->getBills() as $bill) {
            if ($date->isSameDay($bill->getCreatedAt())) {
                $billInfo .= $bill->getInfo();
            }
        }

        return $billInfo;
    }
}