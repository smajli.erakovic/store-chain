<?php

namespace Src\Interfaces;

interface ProductInterface
{
    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getPrice();

    /**
     * @param mixed $price
     * @return Product
     */
    public function setPrice($price);
}